import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
  Example3({Key? key}) : super(key: key);


  final titles = ['sunflower', 'white', 'frangipani', 'needle',
    'rose', 'yellow', 'light orage', 'purple', 'snow'];

  final leading = ['assets/pic/1.jpg', 'assets/pic/2.jpg',
    'assets/pic/3.jpg', 'assets/pic/4.jpg', 'assets/pic/5.jpg',
    'assets/pic/6.jpg', 'assets/pic/7.jpg', 'assets/pic/8.jpg',
    'assets/pic/9.jpg', 'assets/pic/10.jpg'];

  final subtitle =['flower','flower','flower', 'flower','flower','flower','flower',
    'flower','flower','flower',];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ListView3"),
      ),
      body: ListView.builder(
        itemCount: titles.length,
          itemBuilder: (context,index){
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(backgroundImage: AssetImage('${leading[index]}'), radius: 40,),
                  title: Text('${titles[index]}', style: TextStyle(fontSize: 18),),
                  subtitle: Text('${subtitle[index]}', style: TextStyle(fontSize: 15),),
                  trailing: Icon(Icons.notifications_none, size: 25,),
                ),
                Divider(thickness: 1,),
              ],
            );
       },

      ),
    );
  }
}
