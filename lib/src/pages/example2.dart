import 'package:flutter/material.dart';

class Example2 extends StatelessWidget {
  const Example2({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Listview2"),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
                Icons.sunny,
            size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
            size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.diamond,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.dining,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.beach_access_rounded,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.cake,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.dining_outlined,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_boat_filled,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_bus,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_car_outlined,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.directions_transit_sharp,
              size: 25,
            ),
            title: Text(
              "8.00 A.M",
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              "Hello world xxxx.",
              style: TextStyle(fontSize: 15),
            ),
            trailing: Icon(Icons.notifications_none,
              size: 25,
            ),
            onTap: (){
              print("Train");
            },
          ),
        ],
      ),
    );
  }
}
